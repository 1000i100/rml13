# Monnaies libres : tous égaux !

Le terme **monnaie libre** est utilisé selon la définition qu'en fait Stéphane Laborde dans son ouvrage la [Théorie Relative de la Monnaie (TRM)](http://www.creationmonetaire.info/2012/11/theorie-relative-de-la-monnaie-2-718.html).

Les monnaies libres reposent, entre autres principes, sur la notion de symétrie : spatiale et temporelle.

## Principes de symétrie

**Objectif :** Permettre à chaque individu utilisant une monnaie libre d'avoir le même accès à la monnaie que les autres.

### Symétrie spaciale

**Contexte :** Dans les monnaies classiques (€, $, ...), les individus ayant pouvoir de création monétaire
(banquiers entre autres) ont un accès très privilégié à la monnaie comparé aux autres.
Voici quelques articles et vidéos expliquant en quoi.

[Cursus vidéo : Comprendre les monnaies libres](http://www.fdlm.eu/index.php/comprendre-les-monnaies-libres/)

[![La Théorie Relative de la Monnaie par S. Laborde (2014)](img/video_trm_2014.jpg)](https://youtu.be/PdSEpQ8ZtY4)
<br/>[La Théorie Relative de la Monnaie par S. Laborde (2014)](https://youtu.be/PdSEpQ8ZtY4)


**Principe :**
Dans une monnaie libre, chaque création monétaire est distribuée à parts égales entre chaque individu de l'espace économique¹.
Cette égalité de traitement est appelée **symétrie spatiale**.

¹ *Dans le cas de la June (Ǧ1), première monnaie libre, un individu est considéré comme faisant partie
de l'espace économique de la Ǧ1 lorsqu'il en devient membre, c'est-à-dire après avoir été reconnu comme
n'ayant qu'un seul compte membre² Ǧ1 représentant son identité d'humain vivant.
Cette reconnaissance est apportée par des certifications émises par des membres existants.
Il en faut au moins 5 pour être reconnu en tant que membre de l'espace monétaire (et pouvoir certifier à son tour).
La cooptation est le mécanisme choisi dans la Ǧ1 pour ne pas se référer à un pouvoir central comme mécanisme
d'identification unique des individus.
Cette cooptation se différencie fondamentalement des mécanismes de parainage des chaînes et pyramides de Ponzi,
en effet **certifier n'apporte aucun avantage au certifieur**. Une fois membre, chacun est logé à la même enseigne.*

² *Le compte membre se doit d'être unique par individu (personne physique) pour légitimer sa part égale
de création monétaire. En outre, il est possible de créer à loisir de simples comptes portefeuilles.
Ces derniers n'influent pas sur la création monétaire, ils se contentent de stocker la monnaie qu'on y transfert,
tel un compte courant. Les personnes morales (entreprises, associations) peuvent créer des comptes portefeuilles.*

### Symétrie temporelle

**Contexte :**
Les asymétries temporelles des monnaies sont moins évidentes à identifier :
- Pour des monnaies étatiques, il s'agit des variations entre des périodes de fort accroissement du volume monétaire
et des périodes de stagnation, il en va de même pour les MCL (Monnaies Complémentaires Locales), dépendantes de l'euro
(ou autre monnaie étatique) pour leur émission et valorisation.
- Pour les SEL (Systèmes d'Échanges Locaux), l'asymétrie se trouve généralement dans l'apport monétaire à l'arrivée d'un nouveau membre.
En effet, si chaque membre entre dans le SEL avec 10 (ou une autorisation de découvert de 10), le 2nd membre double la masse monétaire (10+10),
le 100ème membre ne l'augemente que d'1% (1000+10). Plus le temps passe, plus la création monétaire devient anecdotique
au regard de l'existant, ce sont donc les premiers entrants et leurs échanges qui ont façonné les rapports de pouvoir
que subiront les suivants, et le mécanisme s'accentue de génération en génération avec les héritages.

C'est pour éviter ce phénomène qu'a été imaginée la notion de symétrie temporelle.

**Principe :**
Dans les monnaies libres, la création monétaire est régulière dans le temps : chaque jour (mois ou année selon la périodicité décidée) un pourcentage fixe de l'ensemble de la monnaie existante est créé
et réparti également entre chaque individu¹. Ainsi la part de monnaie créée par intervalle de temps reste égale, même entre les générations.
C'est la **symétrie temporelle**.
