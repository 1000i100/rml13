# Accompagner la Transition

Transition écologique, énergétique, climatique, citoyenne, sociale, économique, démocratique...

Autant de transitions qu'une monnaie libre et citoyenne peut accompagner.


## Par les citoyens
Aujourd'hui, les choix d'investissements (de création monétaire par accord de prêt pour un projet)
sont basés sur les chances de retour sur investissement, de profit, sur le fait que des fonds alloués à un projet
reviennent le plus certainement possible, et sur l'augmentation des intérêts convenus au moment du prêt...
Ou des dividendes pour satisfaire les investisseurs.

Avec une monnaie libre, la création monétaire se fait exclusivement en créditant chaque citoyen
d'une part égale de la monnaie.
Le pouvoir d'investissement est donc réparti à part égale entre chaque citoyen.
Les choix de financer ou non un projet sont donc le fruit de la volonté
populaire, plutôt que d'un culte au profit.
 
Ainsi le pouvoir monétaire citoyen accompagne la prise de conscience des besoins de transition et leur mise en place.

Les pouvoirs des banques, actionnaires et autres acteurs des marchés financiers
sont transférés aux citoyens par la création monétaire.
Un pouvoir collossal pour agir et faire changer la société dans laquelle nous vivons tous.

## En tous temps
En sus, grâce à la symétrie temporelle, le pouvoir monétaire est également mieux réparti
sur la pyramide des âges et limite l'impact de l'héritage dans le temps.
Ainsi, les jeunes, tournés vers l'avenir, peuvent autant impacter la société que les plus âgés.

Dans la June (Ǧ1), monnaie libre dont la masse monétaire augmente de 10% par an, l'impact monétaire d'un héritage
est dilué par 2 en 7 ans, par 10 en 24 ans, par 100 en 80 ans.
Ce sont donc principalement les choix des vivants qui façonnent le présent,
et cela restera le cas pour les générations futures.

## Sobriété énergétique
La June (Ǧ1), pour son fonctionnement repose sur un réseau pair à pair (p2p, décentralisé, acentré) de blockchain.
Contrairement au Bitcoin, [cette blockchain a été conçue pour être peu énergivore](https://duniter.org/fr/duniter-est-il-energivore/), tout en respectant les principes d'égalité
d'une monnaie libre. Cela est rendu possible par le mécanisme de toile de confiance qui permet à chacun de contribuer
à sa mesure, harmonieusement, sans course à la puissance. Et parce qu'il est toujours possible de faire mieux,
[des évolutions sont à l'étude](https://fygg.nanocryk.fr/t/objectifs-pour-fygg/38) pour rendre cette blockchain encore plus sobre en calcul et en volume.


## Pour approfondir :
En quoi mieux répartir le pouvoir est largement favorable à la Transition ?

Simple : Aujourd'hui, les choix d'investissement sont fait par les pollueurs, pas par les pollués.

![Pourcentage des émissions de CO₂ dans la population mondiale](img/oxfam-co2-richesse.png)

Infographie extraite du rapport de l'OXFAM du 2 décembre 2015

[Inégalités extrêmes et émissions de CO₂](https://www.oxfam.org/sites/www.oxfam.org/files/file_attachments/mb-extreme-carbon-inequality-021215-fr.pdf)

<!--
Les choix les plus impactant sont fait par les riches, dont le mode de vie est le plus polluant.
Les pauvres au contraire son les plus impacté par la pollution, et donc les mieux placé pour avoir conscience de l'ampleur
et de l'urgence des changements à opérer.
-->
