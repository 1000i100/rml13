# Éviter les inégalités monétaires systémiques

Vous êtes banquier ? Trader ? Vous travaillez en centre d'appel ?
Vous cultivez votre potager ou élevez des chèvres dans le Larzac ?
Vous œuvrez bénévolement pour ce qui fait sens à vos yeux ?
Vous êtes citoyen Français, sans-papier, Brésilien, Camrounais, Thaïlandais ?
Homme, femme, blanc, noir, jeune, vieux, autre ?

Si vous êtes humain, avec une monnaie libre, vous avez le même droit de création monétaire que les autres.

Qu'est ce que ça change ?  
 
Le rapport à l'argent, pour tout le monde.

## Travail et délocalisation

Si vous créez une même part de monnaie que n'importe qui, où qu'il soit sur la planète, et que cette création monétaire
à elle seule suffit à doubler la masse monétaire globale en 7 ans environ, serez-vous prêt à travailler pour
un employeur qui vous propose moins que ce à quoi vous avez accès du simple fait d'être humain dans une monnaie libre ?
Probablement pas. Et cela, que vous soyez aux USA, en Inde ou en Malaisie.
Quel serait le prix pour décider à votre place de l'usage de votre temps de vie ? Probablement sensiblement le même que
celui d'un autre, où qu'il soit sur la planète ; finis la délocalisation et le dumping social.

## Financement

Actuellement (en euro) si une banque voit d'un bon œil un projet, elle peut le financer en lui accordant un prêt
(donc en créant de la monnaie¹). Autant dire que si vous êtes de la maison, avec un environnement social qui vous permet
facilement de savoir monter des dossiers qui rentrent dans les cases et que vous présentez bien
(homme, blanc, dans la force de l'âge, si possible avec un patrimoine saisissable juste au cas-où...)
vous serez largement avantagé.

¹ *la monnaie ainsi créée sera certes détruite un jour, mais à un instant T on peu constater
que la très grande majorité de la monnaie en circulation est émise de cette manière, son impact est donc loin d'être
anodin, et cela, sans même parler des intérêts... Quelques vidéos sur le sujet :
[les banques](https://www.youtube.com/watch?v=0dK1yBILG58), [la dette](https://www.youtube.com/watch?v=M60zvmGbK7Y)*


En monnaie libre, puisque la création monétaire vient exclusivement de l'humain, à égalité entre chaque individu,
financer un projet qui nécessite plus que ce que ce dont vous disposez individuellement nécessite... une mutualisation,
l'adhésion de suffisamment d'humains : ce qu'on appelle crowdfunding ou financement participatif.

Vu les codes culturels et les inégalités de visibilité médiatique, il n'est pas dit que cela mettre fin aux inégalités
"à la tête du client". Cela risque juste de rendre ces inégalités représentatives des préjugés d'une population, plutôt
que des préjugés d'un banquier. A première vue, ça me semble un moindre mal, tout en restant loin d'être idéal.

## Géopolitique

Entre pays, zones culturelles, voire continents, cela change déjà plus.
En effet, aujourd'hui l'Occident (Amérique du Nord et Europe) finance la majorité des projets sur son territoire,
mais aussi en Amérique du Sud, en Afrique et en pour une part décroissante en Asie.
Cela, entre autre par colonialisme économique, en ayant la main sur les monnaies utilisées dans ces pays,
et en ayant leur propre monnaie comme référence des échanges internationaux.

Pour autant, l'Occident est loin d'être l'espace le plus peuplé avec ses
[1,3 milliards d'habitants](https://en.wikipedia.org/wiki/List_of_continents_by_population).

En monnaie libre, son pouvoir de financement serait proportionnel à sa population, difficile donc de s'occuper
de son territoire et de celui des autres sans avoir plus de capacité monétaire que les autres.
Les populations qui aujourd'hui se voient imposer des projets décidés par des puissances étrangères auraient cette fois
les moyens de réagir, de financer leurs propres projets, voire de reprendre possession en douceur
de ceux implantés sur leur territoire par des rachats
(la fuite de capitaux correspondant aux rachats étant diluée dans le temps par la création monétaire uniformément
répartie sur la population).


## Monnaie libre unique ? Pas nécessairement.

Si personne n'utilise une monnaie, quelle valeur lui accorder ? Probablement aucune,
puisque vous ne pourrez rien en faire.

Si la grande majorité des individus d'un territoire économiquement autonome (ou presque) utilise une monnaie A alors
sa valeur d'usage sur ce territoire est "pleine" et sera donc peu influencée par son usage hors territoire.

- Prenons deux vastes territoires essentiellement autonomes utilisant chacun une monnaie largement répandue chez eux
- Admettons que ces monnaies soient analysables en ayant des règles de fonctionnement publiquement connues et vérifiables
- Imaginons que ces monnaies intègrent un mode de création monétaire qui maintient la souveraineté monétaire au peuple
de son territoire (et donc lisse les écarts de richesse dans le temps)

Alors, en utilisant une unité de mesure adaptée (invariant économique),
on pourrait comparer ces deux monnaies et établir un taux de change stable.

Si les règles de fonctionnement de ces deux monnaies sont proches, alors ce taux de change, exprimé avec
une unité de mesure adaptée, devrait être de l'ordre de 1 pour 1.

Ainsi, la part de création monétaire de chaque individu dans une monnaie libre largement utilisée
sur un territoire essentiellement autonome, cette part donc, le DU, devrait avoir la même valeur d'usage sur son territoire
(et donc de change entre territoires) que le DU d'un autre territoire essentiellement autonome.
Cela, pour peu que l'augmentation de la masse monétaire annuelle choisie dans chaque territoire soit la même.
Si elle diffère, il y aura probablement un ratio à appliquer lors du change, mais qui pourra rester stable
grâce à la présence d'une unité de mesure stable : le DU.


**Résumé / Conclusion**

En me basant, comme socle théorique, sur la Théorie Relative de la Monnaie, deux monnaies libres partageant
les mêmes réglages et étant largement utilisées devraient avoir la même valeur exprimée en DU, et donc pouvoir s'échanger
avec un ratio de 1 pour 1, en référenciel relatif (DU).

Ainsi plusieurs monnaies libres, chacune apportant la souveraineté monétaire à la population qu'elle sert,
apporteraient, par leur convertibilité stable exprimée en DU, les avantages systémiques décrits plus haut,
à l'échelle globale, alors même qu'une monnaie unique n'est pas adoptée par tous.
