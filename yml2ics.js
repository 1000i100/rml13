const ical = require('ical-generator');
const yaml = require('js-yaml');
const fs = require('fs');
const {addSeconds} = require('date-fns');
const moment = require('moment-timezone');
const md = require('marked');


const data = yaml.safeLoad(fs.readFileSync('generated.programme.yml', 'utf8'));
const conf = yaml.safeLoad(fs.readFileSync('event.config.yml', 'utf8'));

function timeUnit(str){
	const res = {h:0,m:0,s:0};
  if(typeof str !== 'string') return res; //FIXME echou silentieusement
	if(str.indexOf('h')>0) {
		res.h = parseInt(str.split('h')[0].trim());
		res.m = parseInt(str.split('h')[1].split('m')[0].trim())||0;
		str = str.split('h')[1];
	}
	if(str.indexOf('m')>0) {
		res.m = parseInt(str.split('m')[0].trim());
		res.s = parseFloat(str.split('m')[1].split('s')[0].trim())||0;
	}
	return res;
}
function hms2s(hms){
	return hms.h*3600+hms.m*60+hms.s;
}
function duration2s (duration){
	if(typeof duration === "string") return hms2s(timeUnit(duration));
	if(typeof duration === "object"){
		let res = 0;
		for (let i in duration) {
			res += hms2s(timeUnit(duration[i]));
		}
		return res;
	}
	return duration;
}
function speakers2attendees(speakers){
	if(!speakers) return [{name:conf.og.default_author,email:conf.contact.email}];
	if(Array.isArray(speakers)){
		return speakers.map( sp=>{return {name:sp.title,email:conf.contact.email}});
	}
	return [{name:speakers.title,email:conf.contact.email}];
}
function buildDescription(ev){
	let res='';
  if(ev.disclaimer) res+= "A noter :\n"+ev.disclaimer+"\n\n";
	if(ev.target) res+= "Public :\n"+ev.target+"\n\n";
	if(ev.goal) res+= "Objectif :\n"+ev.goal+"\n\n";
	if(ev.description) res+= "Résumé :\n"+ev.description+"\n\n";
	res+= `Emplacement :\n ${ev.where.title}\n${ev.where.address}\n${ev.where.web?ev.where.web+"\n":""}${ev.where.description ? ev.where.description + "\n" : ""}\n\n`;
	if(ev.who && Array.isArray(ev.who)) res+= "Intervenants :\n"+ev.who.map(
		(speaker)=>`- ${speaker.title}\n${speaker.description?speaker.description+"\n":''}${speaker.web?speaker.web+"\n":''}`).join("")+"\n";
	if(ev.who && !Array.isArray(ev.who)) res+= `Intervenant :\n${ev.who.title}\n${ev.who.description?ev.who.description+"\n":''}${ev.who.web?ev.who.web+"\n":''}\n`;
	return res;
}
function buildHtmlDescription(ev){
	let res='';
	if(ev.disclaimer) res+= '**A noter :** '+ev.disclaimer+"\n\n";
	if(ev.target) res+= '**Public :** '+ev.target+"\n\n";
	if(ev.goal) res+= '**Objectif :** '+ev.goal+"\n\n";
	if(ev.description) res+= '**Description :** '+ev.description+"\n\n";
	res+= `**Emplacement :** ${ev.where.title}\n${ev.where.address}\n${ev.where.web?ev.where.web+"\n":""}${ev.where.description ? ev.where.description + "\n" : ""}\n\n`;
	if(ev.who && Array.isArray(ev.who)) res+= "**Intervenants :**\n"+ev.who.map(
		(speaker)=>`- ${speaker.title}\n${speaker.description?speaker.description+"\n":''}${speaker.web?speaker.web+"\n":''}`).join("")+"\n";
	if(ev.who && !Array.isArray(ev.who)) res+= `**Intervenant :**\n${ev.who.title}\n${ev.who.description?ev.who.description+"\n":''}${ev.who.web?ev.who.web+"\n":''}\n`;
	return md(res);
}
function computeStartTime(date,startTime,timezone){
	const ymd = date;
	const y = (new Date(ymd)).getFullYear(),
		month = (new Date(ymd)).getMonth(),
		d = (new Date(ymd)).getDate(),
		{h,m,s} = timeUnit(startTime);
	const momentStart = moment.tz([y, month, d, h, m,s],timezone);
	return new Date(momentStart.format())
}
const ics = ical({
	name: conf.titre.tiny,
	domain: "rml13.creationmonetaire.info",
	url: `${conf.canonical}rml.ics`,
	prodId: {
		company: '1forma-tic.fr',
		product: 'yml2ics',
		language: 'FR'
	},
	timezone: 'Europe/Paris'
});
function assembleRoom(room){
	room = JSON.parse(JSON.stringify(room));
	if(!room.parent) return room;
	room.parent.title = `${room.parent.title} > ${room.title}`;
	room.parent.description = `${room.parent.description}\n${room.description}`;
	room.parent.uid = room.uid;
	return assembleRoom(room.parent);
}
for (let dayContent of data.days) {
	for(let stream of dayContent.streams){
		let startTime = computeStartTime(dayContent.date,dayContent.startTime ||0,ics.timezone());
		for (let ev of stream.sessions) {
			if(ev.gotoTime){
				startTime = computeStartTime(dayContent.date,ev.gotoTime,ics.timezone());
				continue;
			}
			if(ev.setTime){
				startTime = computeStartTime(dayContent.date,ev.setTime,ics.timezone());
				continue;
			}
			if(ev.jumpToTime){
				startTime = computeStartTime(dayContent.date,ev.jumpToTime,ics.timezone());
				continue;
			}
			if(ev.time){
				startTime = computeStartTime(dayContent.date,ev.time,ics.timezone());
				continue;
			}
			if(ev.pause){
				startTime = addSeconds(startTime,duration2s(ev.pause||0));
				continue;
			}
			const endTime = addSeconds(startTime,duration2s(ev.duration||0));
			const categories = ev.tags?ev.tags.split(',').map(str=>{return {name:str.trim()}}):[];
			if(ev.type) categories.unshift({name:ev.type});
			ev.where = assembleRoom(stream.room);
			if(ev.ics !== false) ics.createEvent({
				uid: ev.uid,
				stamp: startTime,
				start: startTime,
				end: endTime,
				summary: ev.title,
				description: buildDescription(ev),
				htmlDescription: buildHtmlDescription(ev),
				location: ev.where.title+', '+ev.where.address,
				geo: {lat:ev.where.gps.lat,lon:ev.where.gps.lon},
				//organizer: mergeSpeakers(ev.who),
				attendees: speakers2attendees(ev.who),
				categories: categories,
				url:`${conf.canonical}session_${ev.uid}.html`,
				status: ev.confirmed?'confirmed':'tentative', //confirmed, tentative, cancelled
				lastModified: new Date(),
			});
			startTime = addSeconds(endTime,duration2s(data.config.afterEachPause||0));
		}
	}

}

ics.saveSync("generated.public/rml.ics");
