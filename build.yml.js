const fs = require("fs");

fs.writeFileSync("generated.programme.yml", computeInclusions("programme/main.yml"), "utf8");

function computeInclusions(sourceFile) {
	let path = getPath(sourceFile);
	let fileContent = fs.readFileSync(sourceFile, "utf8");
	fileContent = fileContent.replace(/%include (.+)/g, (match, catched) => computeInclusions(path + catched));
	return fileContent;
}

function getPath(fileName) {
	return fileName.substr(0, fileName.lastIndexOf('/') + 1);
}
