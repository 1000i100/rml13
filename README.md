
l'adaptation du contenu se limite à changer le contenu des dossiers et fichiers : 
- `programme/`
- `template/pages/`
- `template/index.html`
- `event.config.yml`

Et potentiellement changer quelques contenu dans `static/img/`

Le reste devrait rouler avec : 
- pour tester en local : après install de node.js et npm : `npm install` suivi de `npm run watch` dans le dossier du projet.
- pour mettre en prod : un push sur un dépot pour le quel j'ai configuré la bonne CI/CD





# RML 12
- Site accessible à l'adresse : https://rml12.duniter.io/
- Urls de secours : https://rml12.1000i100.fr/
- Urls de secours : http://1000i100.frama.io/rml12/
